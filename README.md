# Prometheus Yggdrasil Exporter

Exposes metrics data from Yggdrasil as a prometheus endpoint.

Has no external dependencies.

## Building

```shell
go build -o yggdrasil_exporter .
```

## Running

Specify the admin endpoint using the -e flag like so:

```shell
./yggdrasil_exporter -e unix:///var/run/yggdrasil.sock
```

## Metrics

You can see the metrics by just using curl

```shell
curl http://localhost:3000/metrics
```

```
yggdrasil_peer_bytes_sent{peer="200:28c9:944b:2376:2017:3250:bdb4:7c0d"} 6054
yggdrasil_peer_bytes_received{peer="200:28c9:944b:2376:2017:3250:bdb4:7c0d"} 4745
```
